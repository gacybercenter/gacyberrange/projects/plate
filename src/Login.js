import logo from './gcc_logo.svg';
import './Login.css';

function Login() {
  return (
    <div className="Login">
      <div className="Login-header">
        <div class="login-box">
          <img className="login-logo" src={logo}></img>
          <form>
            <input type="text" className="login-input" placeholder="username"></input><br></br>
            <input type="password" className="login-input" placeholder="password"></input><br></br>
            <input type="button" className="login-button" value="LOGIN"></input>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;

import './MainPage.css';
import logo from './gcc_logo.svg';

function Nav() {
    return (
        <div className="navbar">
            <ul className="nav-items">
                <img className="logo" src={logo}></img>
                <div className="buttons">
                    <input type="button" className="nav-button" value="Logout"></input>
                    <input type="button" className="nav-button" value="Profile"></input>
                    <img className="nav-profile-pic" src={logo}></img>
                </div>
            </ul>
            {/* <li>Logout Button</li>
            <li>Username or something</li>
            <li>Profile picture</li> */}
        </div>
    )
};

function SetupControls() {
    return (
        <div className="setup-controls">
            <div className="selectors">
                <span>
                    <label className="selector-label" for="groups">GROUP</label>

                    <select className="selector" name="groups">
                        <option value="one">One</option>
                        <option value="two">Two</option>
                        <option value="three">Three</option>
                    </select>
                </span>

                <span>
                    <label className="selector-label" for="ranges">RANGE</label>

                    <select className="selector" name="ranges">
                        <option value="one">One</option>
                        <option value="two">Two</option>
                        <option value="three">Three</option>
                    </select>
                </span>
            </div>
            <div className="deploy-buttons">
                <input id="deploy-button" type="button" value="DEPLOY" />
                <input id="destroy-button" type="button" value="DESTROY" />
            </div>
        </div>
    )
};

function RangeList() {
    return (
        <div className="range-list">
            <ul>
                <li>Range</li>
                <li>Range</li>
                <li>Range</li>
                <li>Range</li>
                <li>Range</li>
            </ul>
        </div>
    )
};

function TopologyMap () {
    return (
        <div className="topology-container">
            <p>Network Topology</p>
            <div className="topology">
                <p>Yoooo</p>
            </div>
        </div>
    )
};

function Body() {
    return (
        <div className="container">
            <div>
                <input type="button" value="Ranges"></input>
                <input type="button" value="Groups"></input>
            </div>
            <div className="tab-contents">
                <div>
                    <SetupControls />
                    <RangeList />
                </div>
                <TopologyMap />
            </div>
        </div>
    )
};

function MainPage() {
    return (
        <div>
            <Nav /> 
            <Body />
        </div>
    )
}

export default MainPage;